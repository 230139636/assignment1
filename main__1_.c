#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
                                         //Just the Libararies I thought I would need for this code
int
main ()
{
  shelly();                             // My shell is called Shelly a
  return 0;
}


int shelly ()
{  char p;       //This is the case switch I used to handle the initial comand;
   char *str;      //Pointer for the first string to be used as an argument by cat or echo
   char *str0;     //The second pointer to be used by cat as its second argument
   char a='n';     //Initializing a character to setup the exit loop out of the shell




   str=malloc(100*(sizeof(char)));                 //Allocating memory to my strings
   str0=malloc(100*(sizeof(char)));                
    
   printf ("Welcome to shell\nxD\n");
   printf ("Enter your command\n");

   scanf ("%c", &p);
   printf ("%c", p);

   printf("if you wanna break out Type Y\n");
   scanf("%s", &a);
  
   printf("Enter your first String");
   scanf("%s",str);

   printf("Enter your Second String");
   scanf("%s",str0);


  if(a=='Y')
  {
      exit(0);                               //Exit from shell
  }

    pid_t pid=fork();                        //Forking and passing the id onto pid

  if (pid == 0)
    {
      printf ("I am Child\n") ;            //The child says hi
      switch(p)                           //I used p as just a character as there was only 3 commands with differnt first       alphabets
                          //If there werwe more I would have used the n'th element which was unique to use in a character switch
        {
        case 'l' :

                char* chrarry[2];                      //Creating a chracter array to handle the command
                                                       // and pass ls command to  the terminal using execv
                chrarry[0] = "ls";
                chrarry[1] = NULL;
                execvp(chrarry[0], chrarry);     //Case for the command ls system( "ls" );
            printf("is a c ls child");

            break;
        case 'c' :

            char* chrarry0[4];                      // case for command cat // Character array to handle commanad and the strings
            chrarry0[0] = "cat";                   
            chrarry0[1] = str;
            chrarry0[2] = str0;
            chrarry0[3] = NULL;                             //
            execvp(chrarry0[0], chrarry0);

            break;
        case 'e' :

            
            char* chrarry1[3];
            chrarry1[0] = "echo";                         // case to handle echo
            chrarry1[1] = str;                            //uses the first string input to echo it via terminal
            chrarry1[2] = NULL;
            execvp(chrarry1[0], chrarry1);
            break;
        default:
            printf("useless");

        }

    }
  else
    {
        if (pid == -1)
        {
            printf ("antiChild\n") ;
        }
        if (pid > 0)
        {
            wait(NULL);
            free(str);
            free(str0);
            printf ("I am Godfather\n");

        }
    }
return 0;
}
